{
  console.clear(); 
  const headingsList = document.querySelectorAll('h1, h2, h3, h4, h5, h6, [role="heading"]');
  const padSpace = "\t\t\t\t\t";
  headingsList.forEach(function name(heading) {
    const argsLog = [];
    const headingVisibility = getComputedStyle(heading)['visibility'];
    const headingDisplay = getComputedStyle(heading)['display'];
    const level = heading.ariaLevel ? heading.ariaLevel : heading.tagName.replace("H","");
    argsLog.push(`%c${padSpace.slice(0,level-1)}${heading.innerText.replace(/^\s(?:.*)\s*$/m,"")}`,`font-size:${1.8/(level*0.5)}rem; border:
    ${level}px solid transparent`, heading);
    if (headingVisibility === 'hidden' || headingDisplay === 'none') {
      argsLog.push("Not visible");
    }
    console.log(...argsLog);
  })
  const done="List headings done";
  done;
}