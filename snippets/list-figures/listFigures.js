{
  console.clear(); 
  const figureWithCaptionList = document.querySelectorAll('figure:has(figcaption)');
  figureWithCaptionList.forEach(function (figure, index) {
    const legend = figure.querySelector("figcaption").textContent;
    const imgAlt = figure.querySelector("img").getAttribute("alt");

    //TODO : gérer role=img & autres tags
    console.group(`Figure ${index + 1}`)
    console.log(`Legend : ${legend}`,`Image alt: ${imgAlt}`, figure)
    console.groupEnd();
  })
  const done="List figure with figcaption done";
  done;
}