{
    console.clear();
    const frames = document.querySelectorAll('iframe:not([aria-hidden="true"]), frame, frameset');
    const CSSvisibleFrames = Array.from(frames).filter((frame)=> {
            const frameDisplay = getComputedStyle(frame)['display'];
            const frameVisibility = getComputedStyle(frame)['visibility'];
        return (frameDisplay !== 'none' && frameVisibility !== "hidden")
    })
    if (CSSvisibleFrames.length !== 0) {
        CSSvisibleFrames.forEach((frame) => {
            console.log(frame);
        })
    } else {
        console.log("No visible frames");
    }
    const done  = "Listing frames done";
    done;
}