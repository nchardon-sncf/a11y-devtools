# Tools for accessibility audits

## Snippets 

Inspired by https://github.com/matuzo/DevToolsSnippets 

These are scripts to be used in the Snippets section of Sources Tab in DevTools (Chromium Browsers) or copy/pasted in the console. 
